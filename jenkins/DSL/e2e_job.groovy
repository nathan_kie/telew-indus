#!groovy

pipelineJob('e2e') {
    parameters {
        choiceParam('DEPLOY_ENV', ['RECC', 'DEV1', 'DEV2', 'RECE'])
    }

    definition {
        cpsScm {
            scm {
                git {
                    branch('*/master')
                    remote {
                        url('https://nathan_kie@bitbucket.org/nathan_kie/telew-indus.git')
                        credentials('bitbucket')
                    }
                }
                scriptPath('./jenkins/Jenkinsfile/e2e.jenkinsfile')
            }
        }
    }
}
