#!groovy

pipelineJob('build-dsl-job') {
  definition {
    cpsScm {
      scm {
        git {
            branch('*/master')
            remote {
                url('https://nathan_kie@bitbucket.org/nathan_kie/telew-maven.git')
                credentials('bitbucket')
            }
        }
        scriptPath('Jenkinsfile')
      }
    }
  }
}
