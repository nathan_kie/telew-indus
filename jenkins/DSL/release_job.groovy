#!groovy

for (project in [
        [
                'name': 'telew',
                'git': 'https://nathan_kie@bitbucket.org/nathan_kie/telew-maven.git',
                'credId': 'bitbucket',
        ]
]) {
    pipelineJob('release') {
        logRotator {
            numToKeep(15)
        }

        environmentVariables {
            env('TYPE', 'release')
        }

        parameters {
            gitParameter {
                name('BRANCH')
                description('Branch for the release')
                type('PT_BRANCH_TAG')
                defaultValue('master')
                branchFilter('origin/(.*)')
                sortMode('DESCENDING_SMART')
                selectedValue('DEFAULT')
                quickFilterEnabled(true)
                listSize('10')
                branch('')
                tagFilter('[0-9]*')
                useRepository(project.git)
            }
        }

        definition {
            cpsScm {
                scm {
                    git {
                        branch('${BRANCH}')
                        remote {
                            url(project.git)
                            credentials(project.credId)
                        }
                        extensions {
                            localBranch('${BRANCH}')
                        }
                    }
                }
                scriptPath('indus/release.jenkinsfile')
            }
        }
    }
}
