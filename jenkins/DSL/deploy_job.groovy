#!groovy

pipelineJob('deploy') {

    parameters {
        gitParameter {
            name('ANSIBLE_BRANCH')
            description('Ansible branch')
            type('PT_BRANCH_TAG')
            defaultValue('master')
            branchFilter('origin/(.*)')
            sortMode('DESCENDING_SMART')
            selectedValue('DEFAULT')
            quickFilterEnabled(true)
            listSize('10')
            branch('')
            tagFilter('[0-9]*')
            useRepository('https://nathan_kie@bitbucket.org/nathan_kie/telew-indus.git')
        }
        choiceParam('DEPLOY_ENV', ['RECC', 'DEV1', 'DEV2', 'RECE'])

        choiceParam('APP_VERSION', ['latest', '0.0.1', '0.0.2', '0.0.3']) // TODO

    }

    definition {
        cpsScm {
            scm {
                git {
                    branch('*/master')
                    remote {
                        url('https://nathan_kie@bitbucket.org/nathan_kie/telew-indus.git')
                        credentials('bitbucket')
                    }
                }
                scriptPath('./jenkins/Jenkinsfile/deploy.jenkinsfile')
            }
        }
    }
}
