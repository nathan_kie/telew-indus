pipelineJob('cron-job') {
  definition {
    disabled(false)
    cpsScm {
      scm {
        git {
            branch('*/master')
            remote {
                url('https://nathan_kie@bitbucket.org/nathan_kie/telew-indus.git')
                credentials('bitbucket')
            }
        }
        scriptPath('./jenkins/Jenkinsfile/cron.jenkinsfile')
      }
    }
  }
}
